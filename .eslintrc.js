module.exports = {
  root: true,
  env: {
    browser: true,
    es2020: true
  },
  extends: [
    "eslint:recommended"
  ],
  globals: {
    DriveApp: "readonly",
    ScriptApp: "readonly",
    Logger: "readonly",
    SpreadsheetApp: "readonly"
  },
  parser: "babel-eslint"
}
